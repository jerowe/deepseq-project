#!/bin/sh

#DEFAULT PATH
PATH=/root/bin/x86_64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

export ANYENV_ROOT=/opt/anyenv
export PYENV_ROOT=/opt/anyenv/envs/pyenv
export PYENV_VERSION="anaconda-2.4.0"
export NODENV_ROOT=/opt/anyenv/envs/nodenv
export NODENV_VERSION="5.2.0"
export PATH=/opt/anyenv/envs/pyenv/bin:/opt/anyenv/envs/nodenv/bin:/opt/anyenv/envs/pyenv/shims:/opt/anyenv/envs/nodenv/shims:/opt/anyenv/bin:$PATH

cd /data/Project_Youssef_Metagenomics-NCS-28/www/Project_Youssef_Metagenomics-NCS-28
forever start --minUptime 1000 --spinSleepTime 1000 ./bin/www
