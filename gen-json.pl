#!/usr/bin/env perl

use JSON::XS;
 
# exported functions, they croak on error
# and expect/generate UTF-8
 
 
# OO-interface

#my @samples = qw(Sample_15-104 Sample_15-83 Sample_AC_13-8 Sample_AC_20-6 Sample_AC_36-1 Sample_AC_8-8 Sample_CC8 Sample_TV2);

my @samples = qw(Sample_MetaG-1
Sample_MetaG-10
Sample_MetaG-11
Sample_MetaG-12
Sample_MetaG-13
Sample_MetaG-14
Sample_MetaG-15
Sample_MetaG-16
Sample_MetaG-17
Sample_MetaG-18
Sample_MetaG-19
Sample_MetaG-2
Sample_MetaG-20
Sample_MetaG-21
Sample_MetaG-22
Sample_MetaG-23
Sample_MetaG-24
Sample_MetaG-25
Sample_MetaG-26
Sample_MetaG-27
Sample_MetaG-28
Sample_MetaG-29
Sample_MetaG-3
Sample_MetaG-30
Sample_MetaG-31
Sample_MetaG-32
Sample_MetaG-33
Sample_MetaG-34
Sample_MetaG-35
Sample_MetaG-36
Sample_MetaG-37
Sample_MetaG-38
Sample_MetaG-39
Sample_MetaG-4
Sample_MetaG-40
Sample_MetaG-41
Sample_MetaG-42
Sample_MetaG-43
Sample_MetaG-44
Sample_MetaG-45
Sample_MetaG-46
Sample_MetaG-47
Sample_MetaG-48
Sample_MetaG-5
Sample_MetaG-6
Sample_MetaG-7
Sample_MetaG-8
Sample_MetaG-9);
 
my $perl_scalar = {'samples' => \@samples, user => 'user2'};
my $coder = JSON::XS->new->ascii->pretty->allow_nonref;
my $json = $coder->encode ($perl_scalar);
print $json;

