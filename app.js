var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var url = require('url');
var json = require('json-file');
var engine = require('ejs-mate');

var routes = require('./routes/index');
var app = express();

// use ejs-mate for all ejs templates:
app.engine('ejs', engine);

var config = json.read(path.join(__dirname + '/config.json'));

var user = config.get('user');
app.locals.config = config;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

//Define middle ware - using base Urls!
var router = express.Router();

app.use(router);
app.use('/'+config.get('baseUrl'), router); // load the router on baseUrl from config

router.use('/static', express.static(__dirname + '/static'));
router.use('/blog', express.static(__dirname + '/blog'));

router.get('/', function(req, res) {
    var body = "Hello World!";
    res.render('home', {});
});

router.get('/home', function(req, res) {
    var body = "Hello World!";
    res.render('home', {});
});


if(config.get('jbrowse')){
    router.get('/jbrowse', function(req, res) {
        res.render('jbrowse', {});
    });
}
else{
    router.get('/jbrowse', function(req, res) {
        res.response('There is no jbrowse session for this project. If you think this is in error, please contact the BioCore');
    });
}

router.get('/samples', function(req, res) {
    res.render('samples', {});
});

router.get('/python', function(req, res) {
    res.render('jupyterhub', {});
});

router.get('/fastqc/:readtype/:sample/:read', function (req, res) {
    res.render('fastqc', {sample: req.params.sample, read: req.params.read, readtype: req.params.readtype});
});

router.use('/', routes);
app.enable('trust proxy');

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
